from django.conf.urls import url
from django.contrib.auth.views import (
    login,
    logout,
    password_change,
    password_change_done,
    password_reset,
    password_reset_done,
    password_reset_confirm,
    password_reset_complete,
)

from account.views import (
    AccountView,
    AccountUpdate,
    ProfileView,
)

app_name = 'account'
urlpatterns = [
    url(r'^$', AccountView.as_view(), name='view'),

    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout, name='logout', kwargs={'next_page': '/'}),

    url(r'^profile/$', ProfileView.as_view(), name='profile'),

    url(r'^password/change/$', password_change, name='password-change',
        kwargs={'post_change_redirect': 'account:password-change-done'}),
    url(r'^password/change/done/$', password_change_done, name='password-change-done'),
    url(r'^password/reset/$', password_reset, name='password-reset',
        kwargs={'post_reset_redirect': 'account:password-reset-done'}),
    url(r'^password/reset/done/$', password_reset_done, name='password-reset-done'),
    url(r'^password/reset/confirm/$', password_reset_confirm, name='password-reset-confirm',
        kwargs={'post_reset_redirect': 'account:password-reset-complete'}),
    url(r'^password/reset/complete/$', password_reset_complete, name='password-reset-complete'),

    url(r'^(?P<username>[^/]+)/$', AccountView.as_view(), name='view-other'),
    url(r'^(?P<username>[^/]+)/edit/$', AccountUpdate.as_view(), name='member-update'),
]
