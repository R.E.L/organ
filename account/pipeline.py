"""
Social Auth pipeline
"""
from django.contrib.auth.models import User


def load_user(backend, response, *args, **kwargs):
    if backend.name == 'github' and response:
        email = response.get('email')
        user = User.objects.get(email=email)
        return {'user': user}
    return
